package com.hcl.lms.exception;

public class UserInsertionException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public UserInsertionException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
