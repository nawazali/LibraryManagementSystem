package com.hcl.lms.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.lms.User;
import com.hcl.lms.service.Dao;

@Service
public class LmsServiceImpl implements LmsService{
@Autowired
Dao dao;
	@Override
	public User insert(User user) {
		User us=dao.add(user);
		return us;
	}

}
