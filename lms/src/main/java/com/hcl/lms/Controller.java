package com.hcl.lms;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.lms.exception.UserInsertionException;
import com.hcl.lms.service.Dao;

@RestController
public class Controller {
	@Autowired
	Dao dao;
	@Autowired
	JdbcTemplate jdbcTemplate;
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Controller.class);
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public User login(){
		User user=new User();
		user.setUserName("fareds");
		user.setEmail("moin@gmail.com");
		user.setPassword("1234");
		try{
			return dao.add(user);
		}catch(UserInsertionException e) {
			logger.error("userinsertion", e);
		}
		return null;
		
	}
	@Value("${jdbc.sql.fetch.user}")
	String sql;
	private List<User> getUserList(){
		List<User> userlist=jdbcTemplate.query(sql, new Object[] {"NAWAZ"}, customerRowMapper);
		return userlist;
		
	}
	private RowMapper<User> customerRowMapper = new RowMapper<User>() {

		public User mapRow(ResultSet rs, int i) throws SQLException {
			User user=new User();
			user.setEmail(rs.getString(2));
			user.setUserName(rs.getString(1));
			user.setPassword(rs.getString(3));
			return user;
			
		}
};
}
