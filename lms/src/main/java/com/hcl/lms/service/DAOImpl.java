package com.hcl.lms.service;

import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.lms.User;
import com.hcl.lms.exception.UserInsertionException;
@Repository
public class DAOImpl implements Dao{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Value("${jdbc.sql.insert.user}")
	String sql;
	@Override
	public User add(User user) {
		
		try {
		//jdbcTemplate.update(sql, new Object[] {user.getUserName(),user.getEmail(),user.getPassword()});
		return user;
		}catch (DataAccessException e) {
			//logger.error("error while doing knockoutcheck for customerId"+e);
			
			throw new UserInsertionException("error while doing knockoutcheck for customerId", e);
		}
		
	}

}
