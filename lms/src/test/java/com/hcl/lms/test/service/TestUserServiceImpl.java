package com.hcl.lms.test.service;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hcl.lms.User;
import com.hcl.lms.dao.LmsServiceImpl;
import com.hcl.lms.exception.UserInsertionException;
import com.hcl.lms.service.DAOImpl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

@RunWith(MockitoJUnitRunner.class)
public class TestUserServiceImpl {
	@Mock
	DAOImpl daoImpl;
	
	@Mock
	User user;
	
	@InjectMocks
	LmsServiceImpl lmsService;
	
	
	@Before
	public void init() {
		user=new User();
		user.setUserName("fareds");
		user.setEmail("moin@gmail.com");
		user.setPassword("1234");
	}
	
	@Test
	public void fetchUser() {
		when(daoImpl.add(user)).thenReturn(user);
		assertEquals(user, lmsService.insert(user));
		verify(daoImpl).add(user);
		 verify(daoImpl, times(1)).add(user);
		// verify(daoImpl, never()).add(user);
		
	}
	
	@Test(expected = UserInsertionException.class)
	   public void testAdd(){
	      //add the behavior to throw exception
	      doThrow(new UserInsertionException("Add operation not implemented", new Throwable()))
	         .when(daoImpl).add(user);

	      //test the add functionality
	      assertEquals(user, lmsService.insert(user)); 
	   }
	
	
	

}
